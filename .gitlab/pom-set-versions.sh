# !/bin/bash
# BIN_DIR=.gitlab/
BINS_DIR="$(dirname $(readlink -f $0))"
BIN_POM_SET_VERSION="${BINS_DIR}/pom-set-version.sh"
BIN_POM_GAV="${BINS_DIR}/pom-gav.sh"
VERSIONS_CONF="${BINS_DIR}/versions.conf"

# Init variables: env first.
if [ "${VERSION}" == "" ]; then
  VERSION="$1"
  shift # shift parameter ($2+ to $1)
fi
if [ ! -f "${ROOT_POM_FILE}" ]; then
  ROOT_POM_FILE="$1" # use $1 as root/pom.xml
  shift # shift parameter ($2 to $1)
fi
if [ -d "${ROOT_POM_FILE}" ]; then
  ROOT_POM_FILE="${ROOT_POM_FILE}/pom.xml" # try to use ROOT_POM_FILE as dir
fi
if [ ! -f "${ROOT_POM_FILE}" ]; then
  ROOT_POM_FILE="$(pwd)/pom.xml" # try to use PWD/pom.xml as pom-file
fi

# Arguments without $1st=ROOT_POM_FILE
XARGS="$*"

[ ! -f "${ROOT_POM_FILE}" ] && { echo "Root pom file '${ROOT_POM_FILE}' (1st parameter) not exist."; exit 1; }
ROOT_POM_FILE="$(dirname $(readlink -f ${ROOT_POM_FILE}))/$(basename ${ROOT_POM_FILE})"

# Query [relative-mvn-modules]
MODULES=$(mvn -q -Dexec.executable="echo" \
              -Dexec.args=' ${project.modules}' \
              --non-recursive org.codehaus.mojo:exec-maven-plugin:1.6.0:exec \
              -f $ROOT_POM_FILE \
        | sed 's/[][,]//g')

# Iterate [relative-mvn-modules] and try to set version (if neccessary)
for module in ${MODULES}; do
  POM_FILE="$(dirname ${ROOT_POM_FILE})/${module}"
  ARTIFACT_ID=$(bash ${BIN_POM_GAV} -f ${POM_FILE} | cut -d: -f2)
  VERSION=$(cat ${VERSIONS_CONF} | grep ${ARTIFACT_ID} | cut -d= -f2)
  if [ "${VERSION}" != "" ]; then
    bash ${BIN_POM_SET_VERSION} ${VERSION} ${POM_FILE} ${XARGS}
  fi
done
