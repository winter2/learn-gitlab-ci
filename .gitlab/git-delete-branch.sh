#!/bin/bash
BINS_DIR="$(dirname $(readlink -f $0))"
BIN_SET_PUSH_USER="${BINS_DIR}/git-set-push-users.sh"

# set/compute BRANCH_TO_DELETE
BRANCH_TO_DELETE_ENV="${BRANCH_TO_DELETE}"
BRANCH_TO_DELETE=""
if [ "$1"         != "" ] ; then
  BRANCH_TO_DELETE="$1"
elif [ "$BRANCH_TO_DELETE_ENV" != "" ] ; then
  BRANCH_TO_DELETE="$BRANCH_TO_DELETE_ENV"
elif [ "$RELEASE" != "" ] ; then
  BRANCH_TO_DELETE="release/$RELEASE"
elif [ "$PATCH" != "" ] ; then
  BRANCH_TO_DELETE="release/$PATCH"
else
  echo "Missing 1st parameter ~ 'BRANCH_TO_DELETE' or [env:BRANCH_TO_DELETE|env:RELEASE|env:BATCH]."
  exit 1
fi

# config gitrepo for push and delete operations
bash ${BIN_SET_PUSH_USER}

# delete the local branch
if git branch | grep ${BRANCH_TO_DELETE} ; then
  git branch -D ${BRANCH_TO_DELETE}
fi
# delete the branch at remote repo
if git ls-remote origin | grep ${BRANCH_TO_DELETE} ; then
  git push origin --delete ${BRANCH_TO_DELETE}
fi
