#!/bin/bash
# Generate 3 row-table from 'versions.conf'
EXEC_DIR="$(dirname $(readlink -f $0))"
VERSIONS_CONF="${EXEC_DIR}/versions.conf"
NEXT_SNAPSHOT_KEY="next.snapshot"
NEXT_RELEASE_KEY="next.tag"
NEXUS="https://ibykus.nexus"
GITLAB="https://ibykus.gitlab"
GROUP_ID="com.ibykus.afw"

# Repeat(char $1, length(string $2))
repeat () {
  CHAR=$1
  WORD=$2
  WORD_CHARS=$(echo "${WORD}" | wc -c)
  STRING=""
  for ((i = 1; i <= WORD_CHARS; i++)) do
    STRING="${STRING}${CHAR}"
  done
  echo "${STRING}"
}

# Generate 3 row-table:
# | Date | Release | artifact.id.|.s |
# |------|:-------:|:-----------:|:-:|
# |$date | $version| $version    |   |
NOW="$(date)"
HEADER="| Date "
HEADER_SEPARATOR="|-$(repeat "-" "date")-"
LINE="| ${NOW} "
for module in $(cat ${VERSIONS_CONF} | grep -v ${NEXT_SNAPSHOT_KEY}) ; do
  ARTIFACT_ID=$(echo ${module} | cut -d= -f1)
  VERSION=$(echo ${module}     | cut -d= -f2)
  LINK="${NEXUS}/${GROUP_ID}/${ARTIFACT_ID}/${VERISION}"
  # Handle 'next.release' column
  if [ "${ARTIFACT_ID}" == "${NEXT_RELEASE_KEY}" ]; then
    ARTIFACT_ID="Release"
    VERSION="v${VERSION}"
    LINK="${GITLAB}/project/-/tags/${VERSION}"
  fi

  HEADER="${HEADER}| ${ARTIFACT_ID} "
  HEADER_SEPARATOR="${HEADER_SEPARATOR}|:$(repeat "-" "${ARTIFACT_ID}"):"
  LINE="${LINE}| [${VERSION}](${LINK}) "
done
# Close rows.
HEADER="${HEADER}|"
HEADER_SEPARATOR="${HEADER_SEPRATOR}|"
LINE="${LINE}|"

# Distinguish with or without header-rows
if ! [[ "$*" =~ "--no-header" ]] ; then
  echo ${HEADER}
  echo ${HEADER_SEPRATOR}
fi
echo ${LINE}
