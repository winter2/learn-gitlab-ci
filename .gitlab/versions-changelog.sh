#!/bin/bash
# Create or update CHANGELOG.md with versions.conf changelog line.
EXEC_DIR="$(dirname $(readlink -f $0))"
CHANGELOG="${EXEC_DIR}/../CHANGELOG.md"
CHANGELOG_LINE_GENERATOR="${EXEC_DIR}/versions-changelog-line.sh"
CHANGELOG_LINE_GENERATOR_WITHOUT_HEADER="${CHANGELOG_LINE_GENERATOR} --no-header"
CHANGELOG_TMP="/tmp/CHANGELOG.md"
REPLACE_LINE_PREFIX="|----"
REPLACED="false"
REPLACED_IS_TRUE="true"

# Iterate every line in CHANGELOG.md and append the firstRow=$1 (above)
replaceFirstRow () {
  newFirstRow="$1"

  echo "" > ${CHANGELOG_TMP}
  while read line; do
    echo ${line} >> ${CHANGELOG_TMP}
    if [ "${REPLACED}" == "${REPLACED_IS_TRUE}" ] ; then
      continue
    elif [[ "${line}" == ${REPLACE_LINE_PREFIX}* ]] ; then
      echo ${newFirstRow} >> ${CHANGELOG_TMP}
      REPLACED="${REPLACED_IS_TRUE}"
    fi
  done <${CHANGELOG}
}

# Create or update CHANGELOG.md
if [ -f "${CHANGELOG}" ] ; then
  # CHANGELOG.md exist
  LINE=$(bash "${CHANGELOG_LINE_GENERATOR_WITHOUT_HEADER}")
  replaceFirstRow "${LINE}"
  cp "${CHANGELOG_TMP}" "${CHANGELOG}"
else
  # CHANGELOG.md not exist
  bash "${CHANGELOG_LINE_GENERATOR}" > ${CHANGELOG}
fi
