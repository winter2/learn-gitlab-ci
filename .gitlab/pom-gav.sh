# !/bin/bash - require mvn
# - 'project.*' standard maven artifacts
# - 'is.tycho=true|any-other-string (default:empty)' to mark parent poms as tycho artifacts
mvn -q \
  -Dexec.executable="echo" \
  -Dexec.args='${project.groupId}:${project.artifactId}:${project.version}:${project.packaging}:${is.tycho}' \
  --non-recursive \
  org.codehaus.mojo:exec-maven-plugin:1.6.0:exec $*
