# !bin/bash - require mvn
IS_TYCHO_KEY="is.tycho"
IS_TYCHO_DEFAULT="false"
IS_TYCHO_TRUE="true"
PACKAGING_OSGI_REGEX="eclipse-"
BINS_DIR="$(dirname $(readlink -f $0))"
BIN_POM_GAV="${BINS_DIR}/pom-gav.sh"
BIN_POM_ARTIFACT_IDS="${BINS_DIR}/pom-artifactid.sh"

# Init variables: env first.
if [ "${VERSION}" == "" ]; then
  VERSION="$1"
  shift # shift parameter ($2+ to $1)
fi
if [ ! -f "${POM_FILE}" ]; then
  POM_FILE="$1" # set ./set-version.sh $1 $2
  shift # shift parameter ($2+ to $1)
fi
if [ -d "${POM_FILE}" ]; then
  POM_FILE="${POM_FILE}/pom.xml" # try to POM_FILE as dir
fi
if [ ! -f "${POM_FILE}" ]; then
  POM_FILE="$(pwd)/pom.xml" # try to use PWD/pom.xml as pom-file
fi

# check exec requirements
[ "$VERSION" == ""   ] && { echo "Version (1nd parameter) is unset.";                 exit 1; }
[ ! -f "${POM_FILE}" ] && { echo "Pom file '${POM_FILE}' (2st parameter) not exist."; exit 2; }
[ ! -f "${BIN_POM_GAV}" ] && { echo "Script '${BIN_POM_GAV}' not exit.";              exit 3; }

# query 'GroupId:ArtifactId:Version:Packaging:IsTycho'
function gav() {
  GAVP=$(bash ${BIN_POM_GAV} -f ${POM_FILE})
  GROUP_ID=$(echo ${GAVP}        | cut -d: -f1)
  ARTIFACT_ID=$(echo ${GAVP}     | cut -d: -f2)
  VERSION_CURRENT=$(echo ${GAVP} | cut -d: -f3)
  PACKAGING=$(echo ${GAVP}       | cut -d: -f4)
  IS_TYCHO=$(echo ${GAVP}        | cut -d: -f5)
  if [ "${IS_TYCHO}" == "\${${IS_TYCHO_KEY}}" ]; then
    IS_TYCHO="${IS_TYCHO_DEFAULT}"
  fi
  echo "${GROUP_ID}:${ARTIFACT_ID}:${VERSION_CURRENT}:${PACKAGING}:${IS_TYCHO}"
}

# Decide which mvn cmdline (tycho or standard) will be used
function mvncommand() {
  MVN=""
  # set mvn command lines
  MVN_STANDARD="mvn ${MAVEN_CLI_OPTS} org.codehaus.mojo:versions-maven-plugin:2.1:set -DnewVersion=$VERSION $*"
  MVN_OSGI="mvn ${MAVEN_CLI_OPTS} org.eclipse.tycho:tycho-versions-plugin:1.4.0:set-version -DnewVersion=$VERSION $*"
  if [ "${IS_TYCHO}" == "${IS_TYCHO_TRUE}" ] || [[ "${PACKAGING}" =~ "${PACKAGING_OSGI_REGEX}" ]]; then
    ARTIFACT_IDS="$(bash ${BIN_POM_ARTIFACT_IDS} -f ${POM_FILE} | xargs echo | sed 's/ /,/g')"
    MVN="${MVN_OSGI} -f ${POM_FILE} -Dartifacts=${ARTIFACT_IDS}"
  else
    MVN="${MVN_STANDARD} -f ${POM_FILE}"
  fi
  echo $MVN
}

MVN_SET_VERSION=$(mvncommand)
MVN_USE_RELEASE="mvn org.codehaus.mojo:versions-maven-plugin:2.1:use-releases -Dincludes=com.ibykus.afw:* $* -f ${POM_FILE}"

echo "Exec: '${MVN_SET_VERSION}' to upgrade $(gav)"
${MVN_SET_VERSION}
${MVN_USE_RELEASE}
