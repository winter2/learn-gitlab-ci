# !/bin/bash
EXEC_DIR="$(dirname $(readlink -f $0))"
VERSIONS_CONF="${EXEC_DIR}/versions.conf"
NEXT_SNAPSHOT_KEY="NEXT_SNAPSHOT"
NEXT_TAG_KEY="NEXT_TAG"

# COM_IBYKUS_AFW -> com.ibykus.afw
function toLowerKey() {
  echo "$1" | tr '[:upper:]' '[:lower:]' | tr '_' '.'
}
# com.ibykus.afw -> COM_IBYKUS_AFW
function toUpperKey() {
  echo "$1" | tr '[:lower:]' '[:upper:]' | tr '.' '_'
}

# Set '.gitlab/versions.conf#$key=$value' with $1=key and $2=value
function confVersionByKey() {
  KEY=$1
  VALUE=$2
  KEY_LOWER=$(toLowerKey ${KEY})

  if grep "${KEY_LOWER}=" ${VERSIONS_CONF} ; then
    sed -i.${KEY}_${VALUE}.bak 's/'${KEY_LOWER}'=.*/'${KEY_LOWER}'='${VALUE}'/g' ${VERSIONS_CONF}
  fi
}

# Iterate [env:key=value] transform any [key to lower-case and '_ -> .'=value]
# and replace corresponding 'artifactId=value' in '.gitlab/versions.conf'
function confArtifactVersionsFromEnv() {
  while IFS='=' read -r -d '' key value; do
    if [ "${key}" == "_" ]; then
      continue; # ignore '_=env-link'
    fi

    confVersionByKey ${key} ${value}
    #key_lower=$(toLowerKey ${key})
    #if grep "${key_lower}=" ${VERSIONS_CONF} ; then
    #  sed -i.${key}.bak 's/'${key_lower}'=.*/'${key_lower}'='${value}'/g' ${VERSIONS_CONF}
    #fi
  done < <(env -0)
}

# Set '.gitlab/versions.conf#next.snapshot=${env:NEXT_SNAPSHOT}' if exist in env
function confNextSnapshotVersion() {
  KEY="${NEXT_SNAPSHOT_KEY}"
  VALUE="${!NEXT_SNAPSHOT_KEY}"
  confVersionByKey ${KEY} ${VALUE}

  for line in $(cat ${VERSIONS_CONF}) ; do
    LINE_KEY=$(echo $line | cut -d= -f1)
    LINE_VALUE=$(echo $line | cut -d= -f2)
    if [ "${LINE_VALUE}" != "${VALUE}" ]; then
      UPPER_KEY=$(toUpperKey ${LINE_KEY})
      sed -i.${UPPER_KEY}_${LINE_VALUE}.bak 's/'${LINE_KEY}'=.*/'${LINE_KEY}'='${VALUE}'/g' ${VERSIONS_CONF}
    fi
  done
}

# Set '.gitlab/versions.conf#next.tag=${env:NEXT_TAG}' if exist in env
function confNextTagVersion() {
  confVersionByKey ${NEXT_TAG_KEY} ${!NEXT_TAG_KEY}
}

if [[ "$*" =~ "--snapshot"]] ; then
  confNextSnapshotVersion
elif [[ "$*" =~ "--tag"]] ; then
  confNextTagVersion
elif [[ "$*" =~ "--release"]] ; then
  confNextTagVersion
  confArtifactVersionsFromEnv
else
  echo "Missing parameter [--snapshot|--tag|--release] - try again '$0'"
fi
