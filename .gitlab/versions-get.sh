#!/bin/bash
EXEC_DIR="$(dirname $(readlink -f $0))"
VERSIONS_CONF="${EXEC_DIR}/versions.conf"
NEXT_SNAPSHOT_KEY="NEXT_SNAPSHOT"
NEXT_TAG_KEY="NEXT_TAG"
KEY_LOWER=""
KEY_UPPER=""
VALUE=""
VALUE_CONF=""
VALUE_ENV=""
VALUE_DEFAULT=""

if [ "$1" == "" ]; then
  echo "Missing 1st parameter SNAPSHOT|RELEASE|ARTIFACT_ID."
  exit 1
fi
KEY_LOWER="$1"

if [ "$2" != "" ]; then
  VALUE_DEFAULT="$2"
fi

# COM_IBYKUS_AFW -> com.ibykus.afw
function toLowerKey() {
  echo "$1" | tr '[:upper:]' '[:lower:]' | tr '_' '.'
}
# com.ibykus.afw -> COM_IBYKUS_AFW
function toUpperKey() {
  echo "$1" | tr '[:lower:]' '[:upper:]' | tr '.' '_'
}

KEY_UPPER=$(toUpperKey ${KEY_LOWER})
VALUE_ENV=${!KEY_UPPER}
VALUE_CONF=$(grep ${KEY_LOWER} ${VERSIONS_CONF} | cut -d= -f2)

# return/echo 1. equal values | 2. default | 3. env value.
if [ "${VALUE_ENV}" == "${VALUE_CONF}" ]; then
  echo "${VALUE_ENV}"
elif [ "${VALUE_DEFAULT}" != "" ]; then
  echo "${VALUE_DEFAULT}"
elif [ "${VALUE_ENV}" != "" ]; then
  echo "${VALUE_ENV}"
else
  echo "${VALUE_CONF}"
fi
