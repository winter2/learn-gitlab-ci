# !/bin/bash
git config http.sslVerify false
git config user.email "${GIT_PUSH_USER_EMAIL:-neo@gitlab.ci}"
git config user.name "${GIT_PUSH_USER_NAME:-Neo Gitlab-ci}"
git remote get-url origin | sed 's,https://.*@,https://'${GIT_PUSH_USER:-nobody:unknown}@',g' | xargs git remote set-url origin
