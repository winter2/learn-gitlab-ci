# AFW Core

* SCM: https://repo.link
* SCM_SVN: svn://repo.link (old world)
  * http://repo.link/viewer (browser)
* ISSUES: ...
* WIKI: ...
* NEXUS: http://nexus.link
* CHANGELOG: [./CHANGELOG.md](./CHANGELOG.md)

## Getting started

```
$ git clone https://ibkgit02/jwinter/com.ibykus.afw.item.bf.pilot.git
$ cd com.ibykus.afw.item.bf.pilot.git
$ mvn clean install -f items/AfwRT_bf/pom.xml -Pclient-rap
```

## Gitlab-CI

Every commit trigger a [gitlab-pipeline](https://ibkgit02/jwinter/com.ibykus.afw.item.bf.pilot/) build. Depending of commit
branch (feature|trunk|release|patch) will different pipelines triggered.

* trunk pipeline : to build+test+deploy every SNAPSHOT version
* feature pipeline: eq. to 'trunk-pipline' but with manual deploy step
* release pipeline: eq. to ''feature-pipeline' but with additional sonar:test and versions:check
* patch pipeline: is equal to 'release-pipeline'

The release+tag prozess follow the [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) rules. For every pipeline exists different use-cases for any developer ready for manual schedule.

<img src="./.img/gitflow.png" width="400" />

* use-case:prepare-feature (on /trunk) - to create feature/branch by ${FEATURE} name
* use-case:next-snapshot (on /trunk or /master) - to update all versions in `versions.conf` by ${SNAPSHOT}
* use-case:prepare-MR (on /feature /release) - to create merge request (MR) for branch
* use-case:prepare-release (on /trunk) - to create a release branch by ${RELEASE} version
* use-case:tag-master (on /release or /patch) - to merge+tag release branch into master
* use-case:tag-only-master (on /master) - to tag master branch
* use-case:changelog (on /release or /patch) - update CHANGELOG.md with current `versions.conf` values
* use-case:merge-master (on /master) - to merge master into trunk

All use-cases are recommendations to simplyfy the git/gitflow usage.They could also be done on client side with any git-client.

<img src="./.img/usecases.png" width="400" />

### Use-case:prepare-feature on /trunk

Optional input parameter `FEATURE` (should set)

* used as branch name `feature/$FEATURE` (e.g. 'FEATURE=123-issue-title')
* default 'FEATURE=from-${GI_COMMIT_SHA_SHORT}'

<details>
  <summary>Same as `git checkout -b feature/123-issue-title` and `git push origin feature/123-issue-title`. Skip pushing if brnach already exist.</summary>
  <img src="./.img/prepare-feature.png" width="400" />
</details>

Knowing errors:

* fail on script failors

### Use-case:next-snapshot on /trunk or /master

Optional input parameter `SNAPSHOT` (should set).

* used to set the next version in pom.xml's
* default `conf:next.snapshot=X.Y.Z-SNAPSHOT` version.

<details>
  <summary>Same as `bash .gitlab/next-snapshot.sh ${SNAPSHOT} path/to/root/pom.xml 'trunk|master'`.</summary>
  <img src="./.img/next-snapshot.png" width="400" />
</details>

* branch `master` will merged into `trunk`
* before `sh .gitlab/pom-set-versions.sh ${SNAPSHOT} path/to/root/pom.xml`

### Use-case:prepare MR on /feature

No input parameter.

Create a Merge Request (MR) for the underlying feature.

<details>
  <summary>Same as `curl -X POST ${URL}/projects/${ID}/merge_requests`</summary>
  <img src="./.img/prepare-mr2.png" width="400" />
</details>

No knowing erros.

### Use-case:prepare-release on /trunk

Optional input parameter:

* `RELEASE=X.Y.Y` to set release version (default `conf:next.release=X.Y.Z`)
* `ARTIFACT_IDS=VERSION` {0,N} to set `conf:artifact.id-VERSION`

<details>
  <summary>Same as `bash .gitlab/prepare-release.sh "${RELEASE}" "${RELEASE_BRANCH}" path/to/root/pom.xml`</summary>
  <img src="./.img/prepare-release.png" width="400" />
</details>

Knowing errors:

* fail on script errors

### Use-case:tag-master on /release or /patch

No input parameter

<details>
  <summary>Same as `bash .gitlab/git-merge.sh "${MASTER}" "${RELEASE_BRANCH}" "${GIT_MASTER_MERGE_STRATEGY}" -m"CI tagged:${TAG} masters"`</summary>
  <img src="./.img/tag-master.png" width="400" />
</details>

Knowing erros:

* merge conflicts in master -> have to be solved manualy in ${RELEASE_BRANCH}.
* e.g rebase to master

### Use-case:tag-only-master on /master

No input parameter

<details>
  <summary>Same as `git tag v${RELEASE}` and `git push v${RELEASE}`</summary>
  <img src="./.img/tag-master.png" width="400" />
</details>

No knowing errors.

### Use-case:changelog on /release or /patch

No input parameter

<details>
  <summary>Same as `bash .gitlab/versions-changelog.sh` to update `./CHANGELOG.md` with version line of `.gitlab/versions.conf`.
</summary>
  <img src="./.img/changelog.png" width="400" />
</details>

No knowing errors.

### Use-case:merge-master on /master

No input parameter

<details>
  <summary>Same as `bash .gitlab/git-merge.sh "${TRUNK}" "${MASTER}" "${GIT_TRUNK_MERGE_STRATEGY}" -m"CI master into trunk"`</summary>
  <img src="./.img/merge-master.png" width="400" />
</details>

Knowing erros:

* merge conflicts in master -> have to be solved manualy in ${TRUNK}.
* e.g rebase to master
